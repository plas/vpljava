#!/bin/bash

export LANG=en_GB.utf8
source ./vpl_environment.sh

# Proovin kompileerida esitatud *.java faile (õigemini kõiki neid faile, mis ei paista olevat testklassid) ...
find . -name "*.java" -not -name "*Test*.java" \
    | xargs javac -d . -encoding UTF-8 -cp .:/usr/share/java/oop/*

# Teen esitatud klassidele esmase kontrolli ...
java -client -Xmx16m -Dfile.encoding=UTF-8 -cp .:/usr/share/java/oop/* ee.ut.cs.vpljava.StructureChecker

# Proovin nüüd teste kompileerida ...
find . -name "*Test*.java" \
    | xargs javac -d . -encoding UTF-8 -cp .:/usr/share/java/oop/*


# create execution script
printf '#!/bin/bash\n' > vpl_execution
printf '. vpl_environment.sh\n' >> vpl_execution
printf 'export LANG=en_GB.utf8\n' >> vpl_execution
printf 'PATH=$PATH:/usr/local/bin\n' >> vpl_execution
printf 'java -client -Xmx16m -Dfile.encoding=UTF-8 -cp .:/usr/share/java/oop/* ee.ut.cs.vpljava.VPLGrader LendudeTest 0.1\n' >> vpl_execution

chmod +x vpl_execution