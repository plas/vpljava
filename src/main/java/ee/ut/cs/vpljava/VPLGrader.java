package ee.ut.cs.vpljava;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.runner.Description;
import org.junit.runner.JUnitCore;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;


public class VPLGrader {
	
	private static final String GRADUAL = "gradual";
	private static final String YESNO = "yesno";
	private static final String NOGRADE = "nograde";
	private static final String PRECHECK = "precheck";
	private static final String GRADUAL_PRECHECK = "gradual-precheck"; // AKT hack
	private static final String GRADUAL_SHORT = "gradual-short";
	private static final String ZERO_COMMENT = "Esitus ei läbi veel ühtegi testi. Palun proovi veel!";
	private static final String NONZERO_COMMENT = "Tubli! Saad lahenduse eest ühe punkti.";
	private static final String ALWAYS_FAIL = "AlwaysFail:";

	public static void main(String[] args) throws IOException {
		// set parameters
		String testClassName = args[0];
		
		final String gradingScheme; // options are "gradual", "yesno", "nograde", "precheck" or a number
		if (args.length >= 2) {
			gradingScheme = args[1];
		} else {
			gradingScheme = "";
		}
		
		Class<?>[] classes;
		if (testClassName.startsWith(ALWAYS_FAIL)) {
			AlwaysFail.failureMessage = testClassName.substring(ALWAYS_FAIL.length()).trim();
			classes = new Class<?>[]{AlwaysFail.class};
		} else {
			// find test classes
			classes = getClassesFromCurrentDir(testClassName);
		}
		
		final double[] gainedPoints = new double[1];
		final double[] missedPoints = new double[1];
		final int[] passedTests = new int[1];
		final int[] missedTests = new int[1];
		final AtomicReference<Failure> currentFailure = new AtomicReference<Failure>();
		
		JUnitCore junit = new JUnitCore();
		junit.addListener(new RunListener() {
			@Override
			public void testStarted(Description description) throws Exception {
				currentFailure.set(null);
			}
			
			@Override
			public void testAssumptionFailure(Failure failure) {
				currentFailure.set(failure);
			}
			
			@Override
			public void testFailure(Failure failure) throws Exception {
				currentFailure.set(failure);
			}
			
			@Override
			public void testFinished(Description description) throws Exception {
				Failure failure = currentFailure.get(); 
				String descString = getTestDescriptionString(description);
				double points = 1.0;
				VPLData grade = description.getAnnotation(VPLData.class);
				if (grade != null) {
					points = grade.points();
				}
				
				
				if (failure == null) {
					gainedPoints[0] += points;
					passedTests[0]++;
					printTestResult(true, description.getMethodName() + " ... OK", descString, 
							getSuccessMessage(description), gradingScheme);
				}
				else if (failure.getException() != null 
							&& failure.getException() instanceof TestingFeedback) {
					String msg = failure.getException().getMessage();
					printTestResult(false, description.getMethodName() + " ... INFO", descString, 
							msg, gradingScheme);
				}
				else {
					missedPoints[0] += points;
					missedTests[0]++;
					String msg = failure.getMessage();
					if ((msg == null || msg.isEmpty())
							&& failure.getException() != null) {
						Throwable exn = failure.getException(); 
						msg = exn.getClass().getSimpleName()
								+ (exn.getMessage() == null ? "": exn.getMessage() + ", " + failure.getException().getMessage());
					}
					printTestResult(false, description.getMethodName() + " ... FAIL", descString, 
							msg, gradingScheme);
				}
				
				clearLastTestDescription(description);
			}
		});
		
		junit.run(classes);
		printGrade(gainedPoints[0], missedPoints[0], passedTests[0], missedTests[0], gradingScheme);



	}

	private static Class<?>[] getClassesFromCurrentDir(String pattern) throws IOException {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		File curdir = Paths.get("").toAbsolutePath().toFile();
		for (String name : TestingUtils.getClassNamesInDirectory(curdir)) {
			if (name.equals(pattern)) {
				try {
					classes.add(Class.forName(name));
					// System.out.println(name);
				} catch (ClassNotFoundException e) {
					throw new RuntimeException(e);
				}
			}
		}

		return classes.toArray(new Class<?>[classes.size()]);
	}
	
	private static void printTestResult(boolean success, String title, String description, String result,
			String gradingScheme) {
		
		if (gradingScheme.equals(PRECHECK) && success) {
			return;
		}
		
		String content = "-" + title + "\n"
				+ ((description == null || description.isEmpty()) ? "" : description + "\n")
				+ ((result == null || result.isEmpty()) ? "" : result + "\n\n");
		
		if (gradingScheme.equals(PRECHECK) || gradingScheme.equals(GRADUAL_PRECHECK) || gradingScheme.equals(GRADUAL_SHORT)) {
			content = "\n" + content;
		} else {
			content = "\n"
					+ "<|--\n" 
					+ content 
					+ "--|>\n\n";
		}
		
		System.out.println(content);
	}
	
	private static String getTestDescriptionString(Description junitDesc) {
		VPLData vplInfo = junitDesc.getAnnotation(VPLData.class);
		if (vplInfo != null && vplInfo.description() != null && !vplInfo.description().isEmpty()) {
			return vplInfo.description();
		}
		
		
		Class<?> cls = junitDesc.getTestClass();
		if (cls != null) {
			try {
				Field descField = cls.getField("lastTestDescription");
				return (String)descField.get(null);
			} catch (Exception e) {
				// do nothing
			}
		}
		
		return "";
	}
	
	private static String getSuccessMessage(Description junitDesc) {
		VPLData vplInfo = junitDesc.getAnnotation(VPLData.class);
		if (vplInfo != null && vplInfo.successMessage() != null && !vplInfo.successMessage().isEmpty()) {
			return vplInfo.successMessage();
		}
		
		
		Class<?> cls = junitDesc.getTestClass();
		if (cls != null) {
			try {
				Field descField = cls.getField("successMessage");
				return (String)descField.get(null);
			} catch (Exception e) {
				// do nothing
			}
		}
		
		return "";
	}
	
	private static void clearLastTestDescription(Description junitDesc) {
		Class<?> cls = junitDesc.getTestClass();
		if (cls != null) {
			try {
				Field descField = cls.getField("lastTestDescription");
				descField.set(null, null);
			} catch (Exception e) {
				// do nothing
			}
		}
	}
	
	private static void printGrade(double gainedPoints, double missedPoints,
			int passedTests, int missedTests, String scheme) {
		if (scheme.equals(NOGRADE) || scheme.equals(PRECHECK)) {
			return;
		}
		
		double grade;
		try {
			double pointsPerTest = Double.parseDouble(scheme);
			grade = passedTests * pointsPerTest;
			
		} catch (NumberFormatException e) {
			// was not a number, check other options 
			// VPL_GRADEMIN=1 and VPL_GRADEMAX=2 represents pass/fail scheme
			double minGrade = Double.parseDouble(System.getenv().get("VPL_GRADEMIN"));
			double maxGrade = Double.parseDouble(System.getenv().get("VPL_GRADEMAX"));
			
			if (scheme.equals(YESNO) 
					|| scheme.isEmpty() && minGrade == 1 && maxGrade == 2) {
				if (gainedPoints > 0 && missedPoints == 0) {
					grade = maxGrade;
				}
				else {
					grade = minGrade;
				}
			}
			else {
				assert scheme.equals(GRADUAL) || scheme.equals(GRADUAL_PRECHECK) || scheme.equals(GRADUAL_SHORT) || scheme.isEmpty();
				assert minGrade == 0.0;
				
				if (gainedPoints == 0) {
					grade = 0;
				}
				else {
					double gainedRatio = gainedPoints / (gainedPoints + missedPoints);
					grade = maxGrade * gainedRatio;
				}
			}
		}
		System.out.println("Grade :=>> " + gradeToString(grade));

		if (scheme.equals(GRADUAL_PRECHECK)) { // AKT hack
			System.out.println("Comment :=>> " + (grade > 0 ? NONZERO_COMMENT : ZERO_COMMENT));
		}
		else if (scheme.equals(GRADUAL_SHORT)) {
			System.out.printf("Comment :=>> %d/%d testi%n", passedTests, passedTests + missedTests);
		}
	}

	public static String gradeToString(double grade) {
		DecimalFormat df = new DecimalFormat("#.#####");
		df.setRoundingMode(RoundingMode.HALF_UP);
		BigDecimal bigGrade = BigDecimal.valueOf(grade); // avoid floating point unrepresentability
		return df.format(bigGrade);
	}
}
