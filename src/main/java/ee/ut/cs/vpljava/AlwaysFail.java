package ee.ut.cs.vpljava;

import static org.junit.Assert.*;

import org.junit.Test;

public class AlwaysFail {
	public static String failureMessage = null;
	
	@Test
	public void test() {
		if (failureMessage == null || failureMessage.trim().isEmpty()) {
			fail();
		} else {
			fail(failureMessage);
		}
	}

}
