package ee.ut.cs.vpljava;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class StructureChecker {
	private final String TEMPLATE_SUFFIX = "_template";
	private final Map<Class<?>, Class<?>> templateMap = new HashMap<>();
	private final boolean ignorePrivates = true;
	private int problemCount = 0;
	
	private void check() throws Exception {
		File curdir = Paths.get("").toAbsolutePath().toFile();
		List<String> templateClasses = TestingUtils
				.getClassNamesInDirectory(curdir).stream()
				.filter(name -> name.endsWith(TEMPLATE_SUFFIX))
				.collect(Collectors.toList());
		
		for (String className : templateClasses) {
			Class<?> templateClass = Class.forName(className);
			Class<?> targetClass = checkFindClass(templateClass);
			this.templateMap.put(templateClass, targetClass);
		}
		
		// Klasside sisu ei ole mõtet võrrelda, kui mõni klass on puudu
		if (problemCount == 0) {
			for (Map.Entry<Class<?>, Class<?>> entry : this.templateMap.entrySet()) {
				if (entry.getValue() != null) {
					compareClasses(entry.getKey(), entry.getValue());
				}
			}
		}
		
		if (problemCount > 0) {
			System.err.println("-Kõik kompileerimisteated");
		}
		
	}
	
	private Class<?> checkFindClass(Class<?> templateClass) {
		String targetClassName = templateClass.getName().substring(0,
				templateClass.getName().length()-TEMPLATE_SUFFIX.length());
		try {
			return Class.forName(targetClassName);
		} catch (ClassNotFoundException e) {
			report("Ei leia "
					+ (templateClass.isInterface() ? "liidest" : "klassi")
					+ " nimega '" + targetClassName 
					+ "'. Kui sa arvad, et sa siiski esitasid selle, siis kontrolli nime õigekirja ning"
					+ " suur- ja väiketähtede korrektset kasutust. "
					+ (templateClass.getPackage() == null 
							? "Kontrolli ka seda, kas " 
								+ (templateClass.isInterface() ? "liides" : "klass") 
								+" asub ikka vaikepaketis (st. faili alguses ei ole package-direktiivi)"
							: "Ära unusta kontrollida ka paketi nime!"));
			return null;
		}
	}

	private void compareClasses(Class<?> templateClass, Class<?> targetClass) {
		String desc = targetClass.getName();
		if (templateClass.isInterface()) {
			check(targetClass.isInterface(), desc + " peab olema liides");
		}
		else {
			check(!targetClass.isInterface(), desc + " peab olema klass, mitte liides");
			
			if (Modifier.isAbstract(templateClass.getModifiers())) {
				check(Modifier.isAbstract(targetClass.getModifiers()), 
						desc + " peab olema abstraktne");
			}
			else {
				check(!Modifier.isAbstract(targetClass.getModifiers()), 
						desc + " peab olema konkreetne klass, mitte abstraktne");
			}
			
			check(targetClass.getSuperclass().equals(translateType(templateClass.getSuperclass())), 
					desc + " ülemklass peab olema " + translateType(templateClass.getSuperclass()));
		}
		
		
		if (!targetClass.isInterface()) {
			for (Constructor<?> constructor : templateClass.getDeclaredConstructors()) {
				if (!constructor.isSynthetic()) { 
					if (!ignorePrivates || !Modifier.isPrivate(constructor.getModifiers())) {
						checkConstructor(templateClass, targetClass, constructor);
					}
				}
			}
		}
		
		for (Method method : templateClass.getDeclaredMethods()) {
			if (!method.isSynthetic() && !method.isBridge()) {
				if (!ignorePrivates || !Modifier.isPrivate(method.getModifiers())) {
					checkMethod(templateClass, targetClass, method);
				}
			}
		}
		
		for (Field field : templateClass.getDeclaredFields()) {
			if (!field.isSynthetic()) {
				if (!ignorePrivates || !Modifier.isPrivate(field.getModifiers())) {
					checkField(templateClass, targetClass, field);
				}
			}
		}
		
		
		Set<Class<?>> targetInterfaces = new HashSet<>(Arrays.asList(targetClass.getInterfaces()));
		for (Class<?> templateImplementedInterface : templateClass.getInterfaces()) {
			Class<?> transInt = translateType(templateImplementedInterface);
			if (!targetInterfaces.contains(transInt)) {
				if (templateClass.isInterface()) {
					report(desc + " ülemliides peab olema " + typeToString(transInt));
				}
				else {
					report(desc + " peab realiseerima liidese " + typeToString(transInt));
				}
			}
		}
		
		// TODO: check type parameters
		
		compareVisibility(templateClass.getModifiers(), targetClass.getModifiers(),
				targetClass, desc);
		
	}

	private void checkConstructor(Class<?> templateClass, Class<?> targetClass,
			Constructor<?> templateConstructor) {
		Constructor<?> targetConstructor;
		
		try {
			targetConstructor = targetClass.getDeclaredConstructor(
					translateTypes(templateConstructor.getParameterTypes()));
		} catch (SecurityException | NoSuchMethodException e) {
			report("Ei leia konstruktorit " 
					+ targetClass.getName() 
					+ formatParamTypes(translateTypes(templateConstructor.getParameterTypes())));
			return;
		}
		
		compareConstructors(templateClass, targetClass, templateConstructor,
				targetConstructor);
	}
	
	private void compareConstructors(Class<?> templateClass, Class<?> targetClass,
			Constructor<?> templateConstructor, Constructor<?> targetConstructor) {
		String constructorDesc = "Konstruktor " + targetClass.getName() 
			+ formatParamTypes(translateTypes(targetConstructor.getParameterTypes()));
		compareVisibility(templateConstructor.getModifiers(), targetConstructor.getModifiers(),
				targetClass, constructorDesc);
	}
	
	
	private void checkMethod(Class<?> templateClass, Class<?> targetClass, Method templateMethod) {
		Method targetMethod;
		try {
			targetMethod = targetClass.getDeclaredMethod(templateMethod.getName(),
					translateTypes(templateMethod.getParameterTypes()));
		} catch (NoSuchMethodException | SecurityException e) {
			for (Method m : targetClass.getDeclaredMethods()) {
				if (m.getName().equals(templateMethod.getName())) {
					report((targetClass.isInterface() ? "Liideses" : "Klassis") 
							+ " " + targetClass.getName() + " on küll meetod nimega '" + templateMethod.getName()
							+ "', aga puudub variant, mille parameetrite tüübid on järgmised: " + 
							formatParamTypes(translateTypes(templateMethod.getParameterTypes())));
					return;
				}
			}
			report("Ei leia "
					+ (targetClass.isInterface() ? "liidesest" : "klassist") 
					+ " " + targetClass.getName() + " meetodit nimega '" + templateMethod.getName() 
					+ "'. Kontrolli nimede õigekirja ja suur- ning väiketähtede korrektset kasutamist");
			return;
		}
		
		compareMethods(templateClass, targetClass, templateMethod, targetMethod);
	}
	
	private void compareMethods(Class<?> templateClass, Class<?> targetClass,
			Method templateMethod, Method targetMethod) {
		String methodDesc = "Meetod " + targetMethod.getName() 
			+ formatParamTypes(translateTypes(targetMethod.getParameterTypes()))
			+ " " 
			+ (targetClass.isInterface() ? "liideses" : "klassis")
			+ " " 
			+ targetClass.getName();
		
		if (Modifier.isStatic(templateMethod.getModifiers())) {
			check(Modifier.isStatic(targetMethod.getModifiers()), methodDesc + " peab olema staatiline");
		}
		else {
			check(!Modifier.isStatic(targetMethod.getModifiers()), methodDesc + " peab olema isendimeetod");
		}
		
		check(translateType(templateMethod.getReturnType()).isAssignableFrom(targetMethod.getReturnType()), 
				methodDesc + ": tagastustüüp peab olema " + translateType(templateMethod.getReturnType())
					+ " (või selle alamtüüp)");
		
		compareVisibility(templateMethod.getModifiers(), targetMethod.getModifiers(),
				targetClass, methodDesc);
		
		// TODO: final
	}
	
	
	private void checkField(Class<?> templateClass, Class<?> targetClass, Field templateField) {
		Field targetField;
		try {
			targetField = targetClass.getDeclaredField(templateField.getName());
		} catch (SecurityException | NoSuchFieldException e) {
			report("Ei leia "
					+ (targetClass.isInterface() ? "liidesest" : "klassist") 
					+ " " + targetClass.getName()
					+ " välja nimega '" + templateField.getName() + "'");
					return;
		}
		
		compareFields(templateClass, targetClass, templateField, targetField);
	}
	
	private void compareFields(Class<?> templateClass, Class<?> targetClass,
			Field templateField, Field targetField) {
		String desc = "Väli " + targetField.getName()
			+ " " 
			+ (targetClass.isInterface() ? "liideses" : "klassis")
			+ " " 
			+ targetClass.getName();
		
		if (Modifier.isStatic(templateField.getModifiers())) {
			check(Modifier.isStatic(targetField.getModifiers()), desc + " peab olema staatiline");
		}
		else {
			check(!Modifier.isStatic(targetField.getModifiers()), desc + " peab olema isendiväli");
		}
		
		check(templateField.getType().isAssignableFrom(targetField.getType()), 
				desc + ": tüüp peab olema " + templateField.getType()
					+ " (või selle alamtüüp)");
		
		compareVisibility(templateField.getModifiers(), targetField.getModifiers(),
				targetClass, desc);
		
		// TODO: final
	}
	
	private void compareVisibility(int templateObjModifiers, int targetObjModifiers, 
			Class<?> relatedClass, String objectDesc) {
		
		if (Modifier.isPrivate(templateObjModifiers)) {
			check(Modifier.isPrivate(templateObjModifiers), objectDesc + " peab olema privaatne");
		}
		else if (Modifier.isProtected(templateObjModifiers)) {
			check(Modifier.isProtected(templateObjModifiers), objectDesc + " peab olema protected");
		}
		else {
			// Be flexible about public/package-private if class is in default package
			if (relatedClass != null && relatedClass.getPackage() == null) {
				check(!Modifier.isPrivate(targetObjModifiers)
						&& !Modifier.isProtected(targetObjModifiers),
						objectDesc + " peab olema nähtav (avalik või paketi nähtavusega)");
			}
			else {
				if (Modifier.isPublic(templateObjModifiers)) {
					check(Modifier.isPublic(targetObjModifiers),
							objectDesc + " peab olema avalik");
				}
				else {
					check(!Modifier.isPrivate(targetObjModifiers)
							&& !Modifier.isProtected(targetObjModifiers)
							&& !Modifier.isPublic(targetObjModifiers),
							objectDesc + " peab olema paketi nähtavusega");
				}
			}
		}
	}
	
	private String formatParamTypes(Class<?>[] types) {
		List<String> parTypeStrings = new ArrayList<>();
		
		for (Class<?> paramType : types) {
			parTypeStrings.add(typeToString(paramType));
		}
		
		return "(" + String.join(", ", parTypeStrings) + ")";
	}
	
	private Class<?>[] translateTypes(Class<?>[] types) {
		Class<?>[] result = new Class<?>[types.length];
		
		for (int i=0; i < types.length; i++) {
			result[i] = translateType(types[i]);
		}
		
		return result;
	}
	
	private Class<?> translateType(Class<?> type) {
		if (templateMap.containsKey(type) && templateMap.get(type) != null) {
			return templateMap.get(type);
		}
		else {
			return type;
		}
	} 
	
	private String typeToString(Class<?> type) {
		String name = type.getName();
		if (name.startsWith("java.lang.")) {
			return name.substring("java.lang.".length());
		}
		else {
			return name;
		}
	}
	
	private void check(boolean correct, String problemMessage) {
		if (!correct) {
			report(problemMessage);
		}
	}

	private void report(String problemMessage) {
		if (this.problemCount == 0) {
			System.err.println("-Eelkontroll");
		}
		
		System.err.println("* " + problemMessage + "\n");
		this.problemCount++;
	}
	
	public static void main(String[] args) throws Exception {
		new StructureChecker().check();
	}
	
}

