package ee.ut.cs.vpljava;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.lang.ProcessBuilder.Redirect;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class TestingUtils {
	public static class ExecutionResult {
		public ExecutionResult(int returnCode, String out, String err) {
			this.out = out;
			this.err = err;
			this.returnCode = returnCode;
		}
		public String out;
		public String err;
		public int returnCode;
		public String debugInfo;
	}

	public static String monospaceBlock(String s) {
		return "\n>" + s.replaceAll("\\r\\n", "\n").replaceAll("\\n", "\n>") + "\n";
	}
	
	public static ExecutionResult runJavaProgramAsSeparateProcess(String className, String... args) throws Exception {
		return runJavaProgramWithInput(className, "", args);
	}
	
	public static ExecutionResult runJavaProgramWithInput(String className, String input, String... args) throws Exception {
	
		List<String> cmdParts = new ArrayList<String>();
		cmdParts.add("java");
		cmdParts.add("-mx128m");
		cmdParts.add("-Dfile.encoding=UTF-8");
		cmdParts.add("-cp");
		cmdParts.add(joinClasspath(".", "bin", "target/classes", "build/classes/main", "out/production/classes"));
		cmdParts.add(className);
		cmdParts.addAll(Arrays.asList(args));
	
		// Prepare input file
		File inputFile = new File("_temp_input_file_");
		FileOutputStream out = new FileOutputStream(inputFile); 
		out.write(input.getBytes("UTF-8"));
		out.close();
		
		ProcessBuilder pb = new ProcessBuilder(cmdParts.toArray(new String[cmdParts.size()]));
		pb.redirectInput(inputFile);
		pb.redirectOutput(Redirect.PIPE);
		pb.redirectError(Redirect.PIPE);
		
		Process proc = pb.start();
		
		if (!input.isEmpty()) {
			// Send as much as possible
			for (byte b : input.getBytes(Charset.forName("UTF-8"))) {
				try {
					proc.getOutputStream().write(b);
				} catch (IOException e) {
					// TODO: in some cases this should fail
					break;
				}
			}
		}
		int returnCode = proc.waitFor();
		ExecutionResult res = new ExecutionResult(returnCode, 
				readAllFromStream(proc.getInputStream()),
				readAllFromStream(proc.getErrorStream()));
		res.debugInfo = String.join(" ", cmdParts);
		return res;
	}


	private static String joinClasspath(String... parts) {
		String sep;
		if (System.getProperty("os.name").startsWith("Windows")) {
			sep = ";";
		} else {
			sep = ":";
		}
		
		StringBuilder sb = new StringBuilder();
		for (String part : parts) {
			if (System.getProperty("os.name").startsWith("Windows")) {
				sb.append(part.replace("/", "\\"));
			} else {
				sb.append(part.replace("\\", "/"));
			}
			sb.append(sep);
		}
		
		if (sb.length() > 0) {
			sb.deleteCharAt(sb.length()-1);
		}
		
		return sb.toString();
	}
	
	private static String readAllFromStream(InputStream stream) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(stream));
		StringBuilder sb = new StringBuilder();
		int ch;
		while ((ch = br.read()) != -1) {
			sb.append((char)ch);
		}
		return sb.toString();
	}
	
	public static interface RunnableEx {
		public void run() throws Exception;
	}

	public static String captureStdout(RunnableEx action) {
		PrintStream originalStdout = System.out;
		try {
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			System.setOut(new PrintStream(output));
			try {
				action.run();
				return output.toString();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		} finally {
			System.setOut(originalStdout);
		}
	}
	
	public static void runWithFakeInput(RunnableEx action, String... inputLines) throws Exception {
		// http://stackoverflow.com/a/1648989/261181
		String data = String.join(System.getProperty("line.separator"), inputLines);
		InputStream oldStdin = System.in;
		try {
			System.setIn(new ByteArrayInputStream(data.getBytes()));
			action.run();
		} finally {
			System.setIn(oldStdin);
		}		
	}
	
	public static void tryCheck(String taskDescription, RunnableEx check) {
		try {
			check.run();
		}
		catch (TestingFeedback f) {
			throw new TestingFeedback(taskDescription + "\n" + f.getMessage());
		}
		catch (AssertionError e) {
			fail(taskDescription + "\n" + e.getMessage());
		}
		catch (Throwable e) {
			fail(taskDescription + "\nSain vea " + e);
		}
	}
	
	
	public static void createFile(String name, String content) throws Exception {
		createFile(name, content, "UTF-8");
	}
	
	public static void createFile(String name, String content, String encoding) throws Exception {
		Writer fw = new OutputStreamWriter(new FileOutputStream(name), encoding);
		fw.write(content);
		fw.close();
	}
	
	
	public static List<String> getClassNamesInDirectory(File dir) {
		List<String> result = new ArrayList<String>();
		
		for (File item : dir.listFiles()) {
			if (item.isDirectory()) {
				for (String subname : getClassNamesInDirectory(item)) {
					result.add(item.getName() + "." + subname);
				}
			} 
			else if (item.getName().endsWith(".class")) {
				String name = item.getName();
				name = name.substring(0, name.length() - ".class".length());
				result.add(name);
			}
		}
		
		return result;
	}
	
	public static Set<String> getNonPrivateFieldNames(Class<?> cls) {
		Set<String> result = new HashSet<>();
		Field[] allFields = cls.getDeclaredFields();
		for (Field field : allFields) {
		    if (!Modifier.isPrivate(field.getModifiers())) {
		        result.add(field.getName());
		    }
		}
		return result;
	}
	
	public static Set<String> getNonPrivateNonProtectedFieldNames(Class<?> cls) {
		Set<String> result = new HashSet<>();
		Field[] allFields = cls.getDeclaredFields();
		for (Field field : allFields) {
		    if (!Modifier.isPrivate(field.getModifiers())
		    		&& !Modifier.isProtected(field.getModifiers())) {
		        result.add(field.getName());
		    }
		}
		return result;
	}
	
	public static Set<String> getStaticFieldNames(Class<?> cls) {
		Set<String> result = new HashSet<>();
		Field[] allFields = cls.getDeclaredFields();
		for (Field field : allFields) {
		    if (Modifier.isStatic(field.getModifiers())) {
		        result.add(field.getName());
		    }
		}
		return result;
	}
	
	public static void checkOnlyPrivateFields(String... classNames) {
		for (String name : classNames) {
			Class<?> cls = checkGetClass(name);
			checkOnlyPrivateFields(cls);
		}
	}
	
	public static void checkOnlyPrivateFields(Class<?> cls) {
		Set<String> nonPrivate = getNonPrivateFieldNames(cls);
		if (!nonPrivate.isEmpty()) {
			fail("Klassis " + cls.getName() + " on mitteprivaatseid välju " + nonPrivate);
		}
	}
	
	public static void checkOnlyPrivateOrProtectedFields(String... classNames) {
		for (String name : classNames) {
			Class<?> cls = checkGetClass(name);
			checkOnlyPrivateOrProtectedFields(cls);
		}
	}
	
	public static void checkOnlyPrivateOrProtectedFields(Class<?> cls) {
		Set<String> names = getNonPrivateNonProtectedFieldNames(cls);
		if (!names.isEmpty()) {
			fail("Klassis " + cls.getName() + " on välju, mis pole ei private ega protected " + names);
		}
	}
	
	public static void checkNoStaticFields(Class<?> cls) {
		Set<String> items = getStaticFieldNames(cls);
		if (!items.isEmpty()) {
			fail("Klassis " + cls.getName() + " on staatilisi välju " + items);
		}
	}
	
	public static Class<?> checkGetClass(String name) {
		try {
			Class<?> result = Class.forName(name);
			if (result.isInterface()) {
				fail("'" + name + "' peab olema klass, mitte liides");
			}
			
			return result;
		} catch (ClassNotFoundException e) {
			fail("Ei leia klassi nimega '" + name + "'");
			return null; // ei jõua siia
		}
	}
	
	public static Class<?> checkGetInterface(String name) {
		try {
			Class<?> result = Class.forName(name);
			if (!result.isInterface()) {
				fail("'" + name + "' peab olema liides, mitte klass");
			}
			
			return result;
		} catch (ClassNotFoundException e) {
			fail("Ei leia liidest nimega '" + name + "'");
			return null; // ei jõua siia
		}
	}

	public static void checkImplements(Class<?> cls, Class<?> intf) {
		if (!getAllInterfaces(cls).contains(intf)) {
			fail("Klass " + cls.getTypeName() + " peab realiseerima liidese "
					+ intf.getTypeName());
		}
	}
	
	public static void checkSuperclass(Class<?> cls, Class<?> superClass) {
		if (!superClass.isAssignableFrom(cls.getSuperclass())) {
			fail("Klassi " + cls.getTypeName() + " ülemklass peab olema "
					+ superClass.getTypeName() + " (või mõni selle alamklass).");
		}
	}

	public static void checkAbstract(Class<?> cls, boolean isAbstract) {
		if (Modifier.isAbstract(cls.getModifiers()) != isAbstract) {
			fail("Klass "
					+ typeToString(cls)
					+ (isAbstract ? " peab olema " : " ei tohi olla ")
					+ "abstraktne");
		}
	}
	
	public static void checkAbstract(Member member, boolean isAbstract) {
		if (Modifier.isAbstract(member.getModifiers()) != isAbstract) {
			fail(getMemberDesc(member)
					+ (isAbstract ? " peab olema " : " ei tohi olla ")
					+ "abstraktne");
		}
	}
	
	public static List<Class<?>> getAllInterfaces(final Class<?> cls) {
		if (cls == null) {
			return null;
		}

		final LinkedHashSet<Class<?>> interfacesFound = new LinkedHashSet<Class<?>>();
		getAllInterfaces(cls, interfacesFound);

		return new ArrayList<Class<?>>(interfacesFound);
	}

	/**
	 * Get the interfaces for the specified class.
	 *
	 * @param cls  the class to look up, may be {@code null}
	 * @param interfacesFound the {@code Set} of interfaces for the class
	 */
	private static void getAllInterfaces(Class<?> cls, final HashSet<Class<?>> interfacesFound) {
		while (cls != null) {
			final Class<?>[] interfaces = cls.getInterfaces();

			for (final Class<?> i : interfaces) {
				if (interfacesFound.add(i)) {
					getAllInterfaces(i, interfacesFound);
				}
			}

			cls = cls.getSuperclass();
		}
	}
	
	public static void checkDeclaredConstructor(Class<?> cls, Class<?>... paramTypes) {
		try {
			cls.getDeclaredConstructor(paramTypes);
		} catch (NoSuchMethodException | SecurityException e) {
			if (paramTypes.length == 0) {
				fail("Klassis '" +cls.getTypeName()+ "' peaks olema ilma parameetriteta konstruktorit");
			}
			fail("Klassis '" +cls.getTypeName()+ "' peaks olema konstruktor, mille argumentide tüübid on "
					+ formatParamTypes(paramTypes));
		}
	}

	public static void checkPublicConstructor(Class<?> cls, Class<?>... paramTypes) {
		try {
			cls.getConstructor(paramTypes);
		} catch (NoSuchMethodException | SecurityException e) {
			if (paramTypes.length == 0) {
				fail("Klassis '" +cls.getTypeName()+ "' peaks olema ilma parameetriteta konstruktorit");
			}
			fail("Klassis '" +cls.getTypeName()+ "' peaks olema konstruktor, mille argumentide tüübid on "
					+ formatParamTypes(paramTypes));
		}
	}

	
	public static Method checkPublicInstanceMethod(Class<?> cls, String name,
			Class<?> returnType, Class<?>... paramTypes) {
		return checkMethod(cls, name, false, false, returnType, paramTypes, false);
	}
		
	public static Method checkDeclaredInstanceMethod(Class<?> cls, String name,
			Class<?> returnType, Class<?>... paramTypes) {
		return checkMethod(cls, name, false, false, returnType, paramTypes, true);
	}
		
	public static Method checkPublicAbstractMethod(Class<?> cls, String name,
			Class<?> returnType, Class<?>... paramTypes) {
		return checkMethod(cls, name, false, true, returnType, paramTypes, false);
	}
		
	public static Method checkDeclaredAbstractMethod(Class<?> cls, String name,
			Class<?> returnType, Class<?>... paramTypes) {
		return checkMethod(cls, name, false, true, returnType, paramTypes, true);
	}
	
	public static void checkHasMainMethod(Class<?> cls) throws Exception {
		checkStaticMethod(cls, "main", Void.TYPE, Class.forName("[Ljava.lang.String;"));		
	}
		
	public static Method checkStaticMethod(Class<?> cls, String name,
			Class<?> returnType, Class<?>... paramTypes) {
		return checkMethod(cls, name, true, false, returnType, paramTypes, true);
	}

	private static Method checkMethod(Class<?> cls, String name, 
			boolean isStatic, boolean isAbstract, Class<?> returnType, Class<?>[] paramTypes,
			boolean isDeclared) {
		
		Method method = checkGetMethod(cls, isDeclared, name, paramTypes);
		
		if (isStatic != Modifier.isStatic(method.getModifiers())) {
			fail(getMemberDesc(method)
					+ " peab olema "
					+ (isStatic ? "staatiline meetod" : "isendimeetod"));
		}
		
		if (isAbstract != Modifier.isAbstract(method.getModifiers())) {
			fail(getMemberDesc(method)
					+ " peab olema "
					+ (isAbstract ? "abstraktne" : "mitteabstraktne"));
		}
		
		if (returnType != null && !returnType.isAssignableFrom(method.getReturnType())) {
			fail(getMemberDesc(method)
					+ " tagastustüüp peab olema " + typeToString(returnType));
		}
		
		return method;
	}
	
	public static Method checkGetDeclaredMethod(Class<?> cls, String name, Class<?>... paramTypes) {
		return checkGetMethod(cls, true, name, paramTypes);
	}
	
	public static Method checkGetPublicMethod(Class<?> cls, String name, Class<?>... paramTypes) {
		return checkGetMethod(cls, false, name, paramTypes);
	}
	
	public static Method checkGetMethod(Class<?> cls, boolean isDeclared, String name, Class<?>... paramTypes) {
		try {
			if (isDeclared) {
				return cls.getDeclaredMethod(name, paramTypes);
			} else {
				return cls.getMethod(name, paramTypes);
			}
		}
		catch (SecurityException e) {
			throw new RuntimeException(e);
		} 
		catch (NoSuchMethodException e) {
			fail((cls.isInterface() ? "Liideses" : "Klassis")
					+ " '" + cls.getTypeName() + "' peab olema meetod nimega '"
					+ name + "', "
					+ (paramTypes.length == 0 ? "millel pole parameetreid"
							: "mille parameetrite tüübid on " + formatParamTypes(paramTypes))
					);
			return null;
		}
	}
	
	private static String getMemberDesc(Member member) {
		Class<?> cls = member.getDeclaringClass();
		String suffix;
		if (member instanceof Method) {
			suffix = "meetod " + member.getName() + formatParamTypes(((Method) member).getParameterTypes()); 
		}
		else if (member instanceof Constructor<?>) {
			suffix = "konstruktor " + formatParamTypes(((Constructor<?>) member).getParameterTypes());
		}
		else if (member instanceof Field) {
			suffix = "väli '" + member.getName() + "'";
		}
		else {
			suffix = "liige '" + member.getName() + "'";
		}
		
		return (cls.isInterface() ? "Liidese" : "Klassi") 
				+ " " + cls.getTypeName() + " "
				+ suffix;
	}
	
	public static void checkIsPrivate(Member member) {
		if (!Modifier.isPrivate(member.getModifiers())) {
            fail(member.getName() + " peaks olema privaatne");
		}
	}

	private static String formatParamTypes(Class<?>[] types) {
		List<String> parTypeStrings = new ArrayList<>();
		
		for (Class<?> paramType : types) {
			parTypeStrings.add(typeToString(paramType));
		}
		
		return "(" + String.join(", ", parTypeStrings) + ")";
	}
	
	private static String typeToString(Class<?> type) {
		String name = type.getTypeName();
		if (name.startsWith("java.lang.")) {
			return name.substring("java.lang.".length());
		}
		else {
			return name;
		}
	}
	
}