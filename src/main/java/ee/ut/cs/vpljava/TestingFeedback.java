package ee.ut.cs.vpljava;

public class TestingFeedback extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public TestingFeedback(String msg) {
		super(msg);
	}

}
