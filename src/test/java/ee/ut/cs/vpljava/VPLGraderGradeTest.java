package ee.ut.cs.vpljava;

import org.junit.Test;

import static org.junit.Assert.*;

public class VPLGraderGradeTest {

    @Test
    public void testGradeToStringInteger() {
        assertGradeToString("0", 0);
        assertGradeToString("1", 1);
        assertGradeToString("2", 2);
        assertGradeToString("10", 10);
        assertGradeToString("15", 15);
    }

    @Test
    public void testGradeToStringFraction() {
        assertGradeToString("0.1", 0.1);
        assertGradeToString("0.5", 0.5);
        assertGradeToString("0.25", 0.25);
        assertGradeToString("0.75", 0.75);
        assertGradeToString("1.25", 1.25);
        assertGradeToString("0.125", 0.125);
        assertGradeToString("125.125", 125.125);
    }

    @Test
    public void testGradeToStringFractionRound() {
        assertGradeToString("0.5", 0.500001);
        assertGradeToString("0.50001", 0.500009);
        assertGradeToString("0.12346", 0.123456789);
    }

    @Test
    public void testGradeToStringFractionInfinite() {
        assertGradeToString("0.33333", 1.0 / 3);
        assertGradeToString("0.66667", 2.0 / 3);
        assertGradeToString("0.16667", 1.0 / 6);
    }

    @Test
    public void testGradeToStringFloatingPoint() {
        assertGradeToString("0.3", 0.3);
        assertGradeToString("0.3", 0.1 + 0.2); // 0.30000000000000004
    }

    @Test
    public void testGradeToStringFractionRoundHalf() {
        assertGradeToString("0.00001", 0.000005);
        assertGradeToString("0.10001", 0.100005);
        assertGradeToString("0.20001", 0.200005);
        assertGradeToString("0.30001", 0.300005);
        assertGradeToString("0.40001", 0.400005);
        assertGradeToString("0.50001", 0.500005);
    }

    private static void assertGradeToString(String expected, double grade) {
        assertEquals(expected, VPLGrader.gradeToString(grade));
    }
}
